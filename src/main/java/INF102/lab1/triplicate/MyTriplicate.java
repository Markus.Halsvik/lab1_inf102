package INF102.lab1.triplicate;
import java.util.List;


public class MyTriplicate<T> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
        int n = list.size();
        for (int i = 0; i < n; i++) {
            T min = list.get(i);
            int ind = i;
            for (int j = i; j < n; j++) {
                if ((int) list.get(j) < (int) min) {
                    min = list.get(j);
                    ind = j;
                }
            }
            list.set(ind, list.get(i));
            list.set(i, min);
        }
        
        for (int i = 0; i < n - 2; i++) {
            if (list.get(i).equals(list.get(i + 1)) && list.get(i + 1).equals(list.get(i + 2))) {
                return list.get(i);
            }
        }
        return null;
    }
}